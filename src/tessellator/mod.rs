mod event_queue;
mod monotone;
mod sweepline;

use self::event_queue::{EventQueue, PendingEdge};
use self::monotone::Side;
use self::sweepline::{ActiveEdge, Sweepline};
use crate::mesh::{Mesh, Vertex};
use crate::path::Command;

#[derive(Clone, Debug, Default)]
pub struct Tessellator {
    event_queue: EventQueue,
    active_edges: Vec<ActiveEdge>,
    left_edges: Vec<ActiveEdge>,
    right_edges: Vec<PendingEdge>,
    tessellator: monotone::Tessellator,
}

impl Tessellator {
    pub fn new() -> Tessellator {
        Tessellator {
            event_queue: EventQueue::new(),
            active_edges: Vec::new(),
            left_edges: Vec::new(),
            right_edges: Vec::new(),
            tessellator: monotone::Tessellator::new(),
        }
    }

    pub fn tessellate<C>(&mut self, commands: C, mesh: &mut Mesh)
    where
        C: FnOnce(&mut dyn FnMut(Command) -> bool),
    {
        self.event_queue.push_events_for_commands(commands);
        while let Some(vertex) = self
            .event_queue
            .pop_events_for_next_vertex(&mut self.right_edges)
        {
            let mut sweepline = Sweepline::new(vertex, &mut self.active_edges);
            sweepline.remove_incident_edges(
                &mut self.left_edges,
                &mut self.right_edges,
                &mut self.event_queue,
            );
            let index = mesh.vertices.len() as u16;
            mesh.vertices.push(Vertex::new(vertex));
            let (mut lower_polygon, mut upper_polygon) = if self.left_edges.is_empty() {
                sweepline.connect_left_vertex(&mut self.tessellator)
            } else {
                sweepline.finish_left_polygons(
                    &mut self.left_edges,
                    index,
                    &mut self.tessellator,
                    mesh,
                )
            };
            if let Some(polygon) = lower_polygon.as_mut() {
                polygon.borrow_mut().push_vertex(Side::Upper, index, mesh);
            }
            if let Some(polygon) = upper_polygon.as_mut() {
                polygon.borrow_mut().push_vertex(Side::Lower, index, mesh);
            }
            if self.right_edges.is_empty() {
                sweepline.connect_right_vertex(vertex, index, lower_polygon, upper_polygon);
            } else {
                sweepline.start_right_polygons(
                    vertex,
                    &mut self.right_edges,
                    index,
                    lower_polygon,
                    upper_polygon,
                    &mut self.tessellator,
                );
            }
            sweepline.intersect_edges(&mut self.event_queue);
        }
    }
}

#[cfg(test)]
mod tests;
