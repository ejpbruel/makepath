use crate::geometry::Line;
use crate::Mesh;
use std::cell::RefCell;
use std::cmp::Ordering;
use std::rc::Rc;

#[derive(Clone, Debug, Default)]
pub struct Tessellator {
    polygons: Vec<Polygon>,
}

impl Tessellator {
    pub fn new() -> Tessellator {
        Tessellator {
            polygons: Vec::new(),
        }
    }

    pub fn start_polygon(&mut self, index: u16) -> Rc<RefCell<Polygon>> {
        let mut polygon = self.polygons.pop().unwrap_or_else(Polygon::new);
        polygon.start(index);
        Rc::new(RefCell::new(polygon))
    }

    pub fn finish_polygon(&mut self, polygon: Rc<RefCell<Polygon>>, index: u16, mesh: &mut Mesh) {
        let mut polygon = Rc::try_unwrap(polygon).unwrap().into_inner();
        polygon.finish(index, mesh);
        self.polygons.push(polygon);
    }
}

#[derive(Clone, Debug)]
pub struct Polygon {
    side: Side,
    indices: Vec<u16>,
}

impl Polygon {
    pub fn new() -> Polygon {
        Polygon {
            side: Side::Lower,
            indices: Vec::new(),
        }
    }

    pub fn start(&mut self, index: u16) {
        self.indices.push(index);
    }

    pub fn finish(&mut self, index: u16, mesh: &mut Mesh) {
        let mut index_0 = self.indices.pop().unwrap();
        while let Some(index_1) = self.indices.pop() {
            mesh.indices.extend(&[index_1, index_0, index]);
            index_0 = index_1;
        }
    }

    pub fn push_vertex(&mut self, side: Side, index: u16, mesh: &mut Mesh) {
        if side == self.side {
            let mut index_0 = self.indices.pop().unwrap();
            while let Some(index_1) = self.indices.last().cloned() {
                match (
                    Line::new(
                        mesh.vertices[index_0 as usize].position.into(),
                        mesh.vertices[index as usize].position.into(),
                    )
                    .partial_cmp_to_point(mesh.vertices[index_1 as usize].position.into())
                    .unwrap(),
                    side,
                ) {
                    (Ordering::Less, Side::Upper)
                    | (Ordering::Equal, _)
                    | (Ordering::Greater, Side::Lower) => break,
                    _ => (),
                }
                self.indices.pop();
                mesh.indices.extend(&[index_1, index_0, index]);
                index_0 = index_1;
            }
            self.indices.push(index_0);
            self.indices.push(index);
        } else {
            let last_index = self.indices.pop().unwrap();
            let mut index_0 = last_index;
            while let Some(index_1) = self.indices.pop() {
                mesh.indices.extend(&[index_1, index_0, index]);
                index_0 = index_1;
            }
            self.side = side;
            self.indices.push(last_index);
            self.indices.push(index);
        }
    }
}

pub type PolygonRef = Rc<RefCell<Polygon>>;

#[derive(Clone, Copy, Debug, PartialEq)]
pub enum Side {
    Lower,
    Upper,
}
