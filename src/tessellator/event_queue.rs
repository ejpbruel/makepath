use crate::geometry::{Conic, Cubic, Line, Quadratic, Vector};
use crate::path::Command;
use std::cmp::Ordering;
use std::collections::BinaryHeap;
use std::mem;

#[derive(Clone, Debug, Default)]
pub struct EventQueue(BinaryHeap<Event>);

impl EventQueue {
    pub fn new() -> EventQueue {
        EventQueue(BinaryHeap::new())
    }

    pub fn push_event(&mut self, vertex: Vector, edge: Option<PendingEdge>) {
        self.0.push(Event { vertex, edge })
    }

    pub fn push_events_for_commands<C>(&mut self, commands: C)
    where
        C: FnOnce(&mut dyn FnMut(Command) -> bool),
    {
        let mut initial_vertex = None;
        let mut current_vertex = None;
        commands(&mut |command| {
            match command {
                Command::MoveTo(p) => {
                    initial_vertex = Some(p);
                    current_vertex = Some(p);
                }
                Command::LineTo(p1) => {
                    let p0 = current_vertex.unwrap();
                    self.push_events_for_edge(Line::new(p0, p1));
                    current_vertex = Some(p1);
                }
                Command::QuadraticTo(p1, p2) => {
                    Quadratic::new(current_vertex.unwrap(), p1, p2).to_lines(1E-3, |edge| {
                        self.push_events_for_edge(edge);
                        true
                    });
                    current_vertex = Some(p2);
                }
                Command::ConicTo(p1, p2, w) => {
                    Conic::new(current_vertex.unwrap(), p1, p2, w).to_lines(1E-3, |edge| {
                        self.push_events_for_edge(edge);
                        true
                    });
                    current_vertex = Some(p2);
                }
                Command::CubicTo(p1, p2, p3) => {
                    Cubic::new(current_vertex.unwrap(), p1, p2, p3).to_lines(1E-3, |edge| {
                        self.push_events_for_edge(edge);
                        true
                    });
                    current_vertex = Some(p3);
                }
                Command::Close => {
                    let p0 = current_vertex.unwrap();
                    let p1 = initial_vertex.unwrap();
                    self.push_events_for_edge(Line::new(p0, p1));
                    current_vertex = Some(p1);
                }
            }
            true
        });
    }

    pub fn pop_events_for_next_vertex(&mut self, edges: &mut Vec<PendingEdge>) -> Option<Vector> {
        let event = self.0.pop()?;
        if let Some(edge) = event.edge {
            edges.push(edge);
        }
        while let Some(next_event) = self.0.peek().cloned() {
            if next_event != event {
                break;
            }
            self.0.pop();
            if let Some(edge) = next_event.edge {
                edges.push(edge);
            }
        }
        Some(event.vertex)
    }

    fn push_events_for_edge(&mut self, edge: Line) {
        let (winding, p0, p1) = match edge.p0.partial_cmp(&edge.p1).unwrap() {
            Ordering::Less => (1, edge.p0, edge.p1),
            Ordering::Equal => return,
            Ordering::Greater => (-1, edge.p1, edge.p0),
        };
        self.push_event(p0, Some(PendingEdge { winding, p1 }));
        self.push_event(p1, None);
    }
}

#[derive(Clone, Copy, Debug)]
pub struct PendingEdge {
    pub winding: i32,
    pub p1: Vector,
}

impl PendingEdge {
    pub fn to_line(self, p0: Vector) -> Line {
        Line::new(p0, self.p1)
    }

    pub fn eq(self, other: PendingEdge, p0: Vector) -> bool {
        self.partial_cmp(other, p0)
            .map_or(false, |ordering| ordering == Ordering::Equal)
    }

    pub fn partial_cmp(self, other: PendingEdge, p0: Vector) -> Option<Ordering> {
        if self.p1 <= other.p1 {
            other
                .to_line(p0)
                .partial_cmp_to_point(self.p1)
                .map(|ordering| ordering.reverse())
        } else {
            self.to_line(p0).partial_cmp_to_point(other.p1)
        }
    }

    pub fn splice(&mut self, mut other: PendingEdge, event_queue: &mut EventQueue) {
        if self.p1 > other.p1 {
            mem::swap(self, &mut other);
        }
        self.winding += other.winding;
        if self.p1 == other.p1 {
            return;
        }
        event_queue.push_event(self.p1, Some(other));
    }
}

#[derive(Clone, Copy, Debug)]
struct Event {
    vertex: Vector,
    edge: Option<PendingEdge>,
}

impl Eq for Event {}

impl Ord for Event {
    fn cmp(&self, other: &Event) -> Ordering {
        self.partial_cmp(other).unwrap()
    }
}

impl PartialEq for Event {
    fn eq(&self, other: &Event) -> bool {
        self.vertex == other.vertex
    }
}

impl PartialOrd for Event {
    fn partial_cmp(&self, other: &Event) -> Option<Ordering> {
        other.vertex.partial_cmp(&self.vertex)
    }
}
