use crate::geometry::Vector;
use crate::mesh::Vertex;
use crate::{Mesh, Path, Tessellator};

#[test]
fn test_path_with_coincident_edges() {
    let mut actual = Mesh::new();
    let mut tessellator = Tessellator::new();
    let mut path = Path::new();
    path.move_to(Vector::new(-1.0, 0.0));
    path.line_to(Vector::new(0.0, -1.0));
    path.line_to(Vector::new(1.0, 0.0));
    path.close();
    path.line_to(Vector::new(1.0, 0.0));
    path.line_to(Vector::new(0.0, 1.0));
    path.line_to(Vector::new(0.0, 1.0));
    path.close();
    tessellator.tessellate(
        |f| {
            path.commands(f);
        },
        &mut actual,
    );
    let expected = Mesh::with_vertices_and_indices(
        vec![
            Vertex::new(Vector::new(-1.0, 0.0)),
            Vertex::new(Vector::new(0.0, -1.0)),
            Vertex::new(Vector::new(0.0, 1.0)),
            Vertex::new(Vector::new(1.0, 0.0)),
        ],
        vec![0, 2, 3, 0, 1, 3],
    );
    assert_eq!(actual, expected);
}

#[test]
fn test_path_with_coincident_faces() {
    let mut actual = Mesh::new();
    let mut tessellator = Tessellator::new();
    let mut path = Path::new();
    path.move_to(Vector::new(-1.0, 0.0));
    path.line_to(Vector::new(0.0, -1.0));
    path.line_to(Vector::new(1.0, 0.0));
    path.line_to(Vector::new(0.0, 1.0));
    path.close();
    path.line_to(Vector::new(0.0, -1.0));
    path.line_to(Vector::new(1.0, 0.0));
    path.line_to(Vector::new(0.0, 1.0));
    path.close();
    tessellator.tessellate(
        |f| {
            path.commands(f);
        },
        &mut actual,
    );
    let expected = Mesh::with_vertices_and_indices(
        vec![
            Vertex::new(Vector::new(-1.0, 0.0)),
            Vertex::new(Vector::new(0.0, -1.0)),
            Vertex::new(Vector::new(0.0, 1.0)),
            Vertex::new(Vector::new(1.0, 0.0)),
        ],
        vec![0, 1, 2, 1, 2, 3],
    );
    assert_eq!(actual, expected);
}

#[test]
fn test_path_with_coincident_vertices() {
    let mut actual = Mesh::new();
    let mut tessellator = Tessellator::new();
    let mut path = Path::new();
    path.move_to(Vector::new(-1.0, -1.0));
    path.line_to(Vector::new(0.0, 0.0));
    path.line_to(Vector::new(-1.0, 1.0));
    path.line_to(Vector::new(1.0, 1.0));
    path.line_to(Vector::new(0.0, 0.0));
    path.line_to(Vector::new(1.0, -1.0));
    path.close();
    tessellator.tessellate(
        |f| {
            path.commands(f);
        },
        &mut actual,
    );
    let expected = Mesh::with_vertices_and_indices(
        vec![
            Vertex::new(Vector::new(-1.0, -1.0)),
            Vertex::new(Vector::new(-1.0, 1.0)),
            Vertex::new(Vector::new(0.0, 0.0)),
            Vertex::new(Vector::new(1.0, -1.0)),
            Vertex::new(Vector::new(1.0, 1.0)),
        ],
        vec![0, 2, 3, 1, 2, 4],
    );
    assert_eq!(actual, expected);
}

#[test]
fn test_contour_with_hole() {
    let mut actual = Mesh::new();
    let mut tessellator = Tessellator::new();
    let mut path = Path::new();
    path.move_to(Vector::new(-2.0, 0.0));
    path.line_to(Vector::new(0.0, -2.0));
    path.line_to(Vector::new(2.0, 0.0));
    path.line_to(Vector::new(0.0, 2.0));
    path.close();
    path.move_to(Vector::new(-1.0, 0.0));
    path.line_to(Vector::new(0.0, 1.0));
    path.line_to(Vector::new(1.0, 0.0));
    path.line_to(Vector::new(0.0, -1.0));
    path.close();
    tessellator.tessellate(
        |f| {
            path.commands(f);
        },
        &mut actual,
    );
    let expected = Mesh::with_vertices_and_indices(
        vec![
            Vertex::new(Vector::new(-2.0, 0.0)),
            Vertex::new(Vector::new(-1.0, 0.0)),
            Vertex::new(Vector::new(0.0, -2.0)),
            Vertex::new(Vector::new(0.0, -1.0)),
            Vertex::new(Vector::new(0.0, 1.0)),
            Vertex::new(Vector::new(0.0, 2.0)),
            Vertex::new(Vector::new(1.0, 0.0)),
            Vertex::new(Vector::new(2.0, 0.0)),
        ],
        vec![
            0, 1, 2, 1, 2, 3, 0, 1, 4, 0, 4, 5, 2, 3, 6, 4, 5, 6, 5, 6, 7, 2, 6, 7,
        ],
    );
    assert_eq!(actual, expected);
}

#[test]
fn test_path_with_intersecting_edges() {
    let mut actual = Mesh::new();
    let mut tessellator = Tessellator::new();
    let mut path = Path::new();
    path.move_to(Vector::new(-1.0, -1.0));
    path.line_to(Vector::new(1.0, -1.0));
    path.line_to(Vector::new(-1.0, 1.0));
    path.line_to(Vector::new(1.0, 1.0));
    path.close();
    tessellator.tessellate(
        |f| {
            path.commands(f);
        },
        &mut actual,
    );
    let expected = Mesh::with_vertices_and_indices(
        vec![
            Vertex::new(Vector::new(-1.0, -1.0)),
            Vertex::new(Vector::new(-1.0, 1.0)),
            Vertex::new(Vector::new(0.0, 0.0)),
            Vertex::new(Vector::new(1.0, -1.0)),
            Vertex::new(Vector::new(1.0, 1.0)),
        ],
        vec![0, 2, 3, 1, 2, 4],
    );
    assert_eq!(actual, expected);
}

#[test]
fn test_path_with_left_vertex() {
    let mut actual = Mesh::new();
    let mut tessellator = Tessellator::new();
    let mut path = Path::new();
    path.move_to(Vector::new(-1.0, 0.0));
    path.line_to(Vector::new(1.0, -1.0));
    path.line_to(Vector::new(0.0, 0.0));
    path.line_to(Vector::new(1.0, 1.0));
    path.close();
    tessellator.tessellate(
        |f| {
            path.commands(f);
        },
        &mut actual,
    );
    let expected = Mesh::with_vertices_and_indices(
        vec![
            Vertex::new(Vector::new(-1.0, 0.0)),
            Vertex::new(Vector::new(0.0, 0.0)),
            Vertex::new(Vector::new(1.0, -1.0)),
            Vertex::new(Vector::new(1.0, 1.0)),
        ],
        vec![0, 1, 2, 0, 1, 3],
    );
    assert_eq!(actual, expected);
}

#[test]
fn test_path_with_right_vertex() {
    let mut actual = Mesh::new();
    let mut tessellator = Tessellator::new();
    let mut path = Path::new();
    path.move_to(Vector::new(0.0, 0.0));
    path.line_to(Vector::new(-1.0, -1.0));
    path.line_to(Vector::new(1.0, 0.0));
    path.line_to(Vector::new(-1.0, 1.0));
    path.close();
    tessellator.tessellate(
        |f| {
            path.commands(f);
        },
        &mut actual,
    );
    let expected = Mesh::with_vertices_and_indices(
        vec![
            Vertex::new(Vector::new(-1.0, -1.0)),
            Vertex::new(Vector::new(-1.0, 1.0)),
            Vertex::new(Vector::new(0.0, 0.0)),
            Vertex::new(Vector::new(1.0, 0.0)),
        ],
        vec![1, 2, 3, 0, 2, 3],
    );
    assert_eq!(actual, expected);
}

#[test]
fn test_path_with_vertical_edges() {
    let mut actual = Mesh::new();
    let mut tessellator = Tessellator::new();
    let mut path = Path::new();
    path.move_to(Vector::new(-1.0, -1.0));
    path.line_to(Vector::new(1.0, -1.0));
    path.line_to(Vector::new(1.0, 1.0));
    path.line_to(Vector::new(-1.0, 1.0));
    path.close();
    tessellator.tessellate(
        |f| {
            path.commands(f);
        },
        &mut actual,
    );
    let expected = Mesh::with_vertices_and_indices(
        vec![
            Vertex::new(Vector::new(-1.0, -1.0)),
            Vertex::new(Vector::new(-1.0, 1.0)),
            Vertex::new(Vector::new(1.0, -1.0)),
            Vertex::new(Vector::new(1.0, 1.0)),
        ],
        vec![0, 1, 2, 1, 2, 3],
    );
    assert_eq!(actual, expected);
}
