use super::event_queue::{EventQueue, PendingEdge};
use super::monotone::{PolygonRef, Tessellator};
use crate::geometry::{Line, Vector};
use crate::Mesh;
use std::cmp::Ordering;
use std::mem;

#[derive(Debug)]
pub struct Sweepline<'a> {
    active_edges: &'a mut Vec<ActiveEdge>,
    vertex: Vector,
    start: usize,
    end: usize,
}

impl<'a> Sweepline<'a> {
    pub fn new(vertex: Vector, active_edges: &'a mut Vec<ActiveEdge>) -> Sweepline {
        let mut start = active_edges
            .iter()
            .position(|edge| edge.edge.partial_cmp_to_point(vertex).unwrap() != Ordering::Less)
            .unwrap_or_else(|| active_edges.len());
        let mut end = active_edges
            .iter()
            .rposition(|edge| edge.edge.partial_cmp_to_point(vertex).unwrap() != Ordering::Greater)
            .map_or(0, |index| index + 1);
        while start > 0 && active_edges[start - 1].is_temporary {
            start -= 1;
            active_edges[start].split(vertex);
        }
        while end < active_edges.len() && active_edges[end].is_temporary {
            active_edges[end].split(vertex);
            end += 1;
        }
        Sweepline {
            vertex,
            active_edges,
            start,
            end,
        }
    }

    pub fn remove_incident_edges(
        &mut self,
        left_edges: &mut Vec<ActiveEdge>,
        mut right_edges: &mut Vec<PendingEdge>,
        event_queue: &mut EventQueue,
    ) {
        left_edges.extend(self.active_edges.drain(self.start..self.end).map({
            let vertex = self.vertex;
            let right_edges = &mut right_edges;
            move |mut edge| {
                if let Some(edge) = edge.split(vertex) {
                    right_edges.push(edge);
                }
                edge
            }
        }));
        self.end = self.start;
        right_edges.sort_by(|&edge_0, &edge_1| edge_0.partial_cmp(edge_1, self.vertex).unwrap());
        let mut index_0 = 0;
        for index_1 in 1..right_edges.len() {
            let edge_1 = right_edges[index_1];
            let edge_0 = &mut right_edges[index_0];
            if edge_0.eq(edge_1, self.vertex) {
                edge_0.splice(edge_1, event_queue);
            } else {
                index_0 += 1;
                right_edges[index_0] = edge_1;
            }
        }
        right_edges.truncate(index_0 + 1);
    }

    pub fn connect_left_vertex(
        &mut self,
        tessellator: &mut Tessellator,
    ) -> (Option<PolygonRef>, Option<PolygonRef>) {
        if self.last_lower_region_winding() == 0 {
            return (None, None);
        }
        let last_lower_edge = &self.active_edges[self.start - 1];
        let last_upper_edge = &self.active_edges[self.end];
        if last_lower_edge.edge.p0 > last_upper_edge.edge.p1 {
            let lower_polygon = tessellator.start_polygon(last_lower_edge.start_index);
            (
                Some(lower_polygon.clone()),
                self.active_edges[self.start - 1]
                    .upper_polygon
                    .replace(lower_polygon),
            )
        } else {
            let upper_polygon = tessellator.start_polygon(last_upper_edge.start_index);
            (
                self.active_edges[self.end]
                    .lower_polygon
                    .replace(upper_polygon.clone()),
                Some(upper_polygon),
            )
        }
    }

    pub fn finish_left_polygons(
        &mut self,
        left_edges: &mut Vec<ActiveEdge>,
        end_index: u16,
        tessellator: &mut Tessellator,
        mesh: &mut Mesh,
    ) -> (Option<PolygonRef>, Option<PolygonRef>) {
        assert!(!left_edges.is_empty());
        let mut upper_edge = left_edges.pop().unwrap();
        let upper_polygon = upper_edge.upper_polygon.take();
        while let Some(mut lower_edge) = left_edges.pop() {
            if lower_edge.upper_region_winding != 0 {
                let polygon = lower_edge.upper_polygon.take().unwrap();
                upper_edge.lower_polygon.take().unwrap();
                tessellator.finish_polygon(polygon, end_index, mesh);
            }
            upper_edge = lower_edge;
        }
        (upper_edge.lower_polygon.take(), upper_polygon)
    }

    pub fn connect_right_vertex(
        &mut self,
        vertex: Vector,
        start_index: u16,
        lower_polygon: Option<PolygonRef>,
        upper_polygon: Option<PolygonRef>,
    ) {
        let last_lower_region_winding = self.last_lower_region_winding();
        if last_lower_region_winding == 0 {
            return;
        }
        self.active_edges.insert(
            self.end,
            ActiveEdge {
                is_temporary: true,
                winding: 0,
                edge: Line::new(
                    vertex,
                    self.active_edges[self.start - 1]
                        .edge
                        .p1
                        .min(self.active_edges[self.end].edge.p1),
                ),
                upper_region_winding: last_lower_region_winding,
                start_index,
                lower_polygon,
                upper_polygon,
            },
        );
        self.end += 1;
    }

    pub fn start_right_polygons(
        &mut self,
        vertex: Vector,
        right_edges: &mut Vec<PendingEdge>,
        start_index: u16,
        lowermost_polygon: Option<PolygonRef>,
        mut uppermost_polygon: Option<PolygonRef>,
        tessellator: &mut Tessellator,
    ) {
        assert!(!right_edges.is_empty());
        let mut lower_region_winding = self.last_lower_region_winding();
        let mut lower_polygon = lowermost_polygon;
        let len = right_edges.len();
        self.active_edges.splice(
            self.end..self.end,
            right_edges.drain(..).enumerate().map(move |(index, edge)| {
                let upper_region_winding = lower_region_winding + edge.winding;
                let upper_polygon = if upper_region_winding == 0 {
                    None
                } else if index == len - 1 {
                    uppermost_polygon.take()
                } else {
                    Some(tessellator.start_polygon(start_index))
                };
                let active_edge = ActiveEdge {
                    is_temporary: false,
                    winding: edge.winding,
                    edge: edge.to_line(vertex),
                    upper_region_winding,
                    start_index,
                    lower_polygon: lower_polygon.take(),
                    upper_polygon: upper_polygon.clone(),
                };
                lower_region_winding = upper_region_winding;
                lower_polygon = upper_polygon;
                active_edge
            }),
        );
        self.end += len;
    }

    pub fn intersect_edges(&mut self, event_queue: &mut EventQueue) {
        if self.start > 0 && self.start < self.active_edges.len() {
            self.intersect_edge_pair(self.start - 1, self.start, event_queue);
        }
        if self.start < self.end && self.end < self.active_edges.len() {
            self.intersect_edge_pair(self.end - 1, self.end, event_queue);
        }
    }

    fn intersect_edge_pair(
        &mut self,
        index_0: usize,
        index_1: usize,
        event_queue: &mut EventQueue,
    ) {
        let edge_0 = self.active_edges[index_0].edge;
        let edge_1 = self.active_edges[index_1].edge;
        if edge_0.p1 == edge_1.p1 {
            return;
        }
        let p = if let Some(p) = edge_0.intersect(edge_1) {
            p
        } else {
            return;
        };
        // let p = (p.clamp(edge_0.bounds().intersect(edge_1.bounds()))).max(self.vertex);
        self.split_edge(index_0, p, event_queue);
        self.split_edge(index_1, p, event_queue);
    }

    fn split_edge(&mut self, index: usize, mut p: Vector, event_queue: &mut EventQueue) {
        if let Some(mut edge) = self.active_edges[index].split(p) {
            if edge.p1 < p {
                edge.winding = -edge.winding;
                mem::swap(&mut edge.p1, &mut p);
            }
            event_queue.push_event(p, Some(edge));
        }
    }

    fn last_lower_region_winding(&self) -> i32 {
        if self.start == 0 {
            return 0;
        }
        self.active_edges[self.start - 1].upper_region_winding
    }
}

#[derive(Clone, Debug)]
pub struct ActiveEdge {
    is_temporary: bool,
    winding: i32,
    edge: Line,
    upper_region_winding: i32,
    start_index: u16,
    lower_polygon: Option<PolygonRef>,
    upper_polygon: Option<PolygonRef>,
}

impl ActiveEdge {
    fn split(&mut self, p: Vector) -> Option<PendingEdge> {
        let p1 = self.edge.p1;
        if p == p1 {
            return None;
        }
        self.edge.p1 = p;
        Some(PendingEdge {
            winding: self.winding,
            p1,
        })
    }
}
