use crate::geometry::{F32Ext, Rectangle, Vector3};
use std::ops::{Add, Div, Mul, Neg, Sub};

#[derive(Clone, Copy, Debug, PartialEq, PartialOrd)]
pub struct Vector {
    pub x: f32,
    pub y: f32,
}

impl Vector {
    pub fn new(x: f32, y: f32) -> Vector {
        Vector { x, y }
    }

    pub fn is_almost_zero(self) -> bool {
        self.length_sqr().abs() <= 1E-9
    }

    pub fn length(self) -> f32 {
        self.length_sqr().sqrt()
    }

    pub fn length_sqr(self) -> f32 {
        self.dot(self)
    }

    pub fn cross(self, other: Vector) -> f32 {
        self.x * other.y - self.y * other.x
    }

    pub fn dot(self, other: Vector) -> f32 {
        self.x * other.x + self.y * other.y
    }

    pub fn normalize(self) -> Option<Vector> {
        if self.is_almost_zero() {
            return None;
        }
        Some(self / self.length())
    }

    pub fn perpendicular(self) -> Vector {
        Vector::new(-self.y, self.x)
    }

    pub fn min(self, other: Vector) -> Vector {
        if self <= other {
            return self;
        }
        other
    }

    pub fn max(self, other: Vector) -> Vector {
        if self <= other {
            return other;
        }
        self
    }

    pub fn clamp(self, bounds: Rectangle) -> Vector {
        Vector::new(
            self.x.max(bounds.min.x).min(bounds.max.x),
            self.x.max(bounds.min.y).min(bounds.max.y),
        )
    }

    pub fn lerp(self, other: Vector, t: f32) -> Vector {
        self + (other - self) * t
    }

    pub fn project_up(self, z: f32) -> Vector3 {
        Vector3::new(self.x * z, self.y * z, z)
    }
}

impl From<[f32; 2]> for Vector {
    fn from(array: [f32; 2]) -> Vector {
        Vector::new(array[0], array[1])
    }
}

impl Into<[f32; 2]> for Vector {
    fn into(self) -> [f32; 2] {
        [self.x, self.y]
    }
}

impl Add for Vector {
    type Output = Vector;

    fn add(self, other: Vector) -> Vector {
        Vector::new(self.x + other.x, self.y + other.y)
    }
}

impl Sub for Vector {
    type Output = Vector;

    fn sub(self, other: Vector) -> Vector {
        Vector::new(self.x - other.x, self.y - other.y)
    }
}

impl Mul<f32> for Vector {
    type Output = Vector;

    fn mul(self, k: f32) -> Vector {
        Vector::new(self.x * k, self.y * k)
    }
}

impl Div<f32> for Vector {
    type Output = Vector;

    fn div(self, k: f32) -> Vector {
        Vector::new(self.x / k, self.y / k)
    }
}

impl Neg for Vector {
    type Output = Vector;

    fn neg(self) -> Vector {
        Vector::new(-self.x, -self.y)
    }
}
