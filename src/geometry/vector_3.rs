use crate::geometry::{F32Ext, Vector};
use std::ops::{Add, Div, Mul, Neg, Sub};

#[derive(Clone, Copy, Debug, PartialEq, PartialOrd)]
pub struct Vector3 {
    pub x: f32,
    pub y: f32,
    pub z: f32,
}

impl Vector3 {
    pub fn new(x: f32, y: f32, z: f32) -> Vector3 {
        Vector3 { x, y, z }
    }

    pub fn length(self) -> f32 {
        self.length_sqr().sqrt()
    }

    pub fn length_sqr(self) -> f32 {
        self.dot(self)
    }

    pub fn dot(self, other: Vector3) -> f32 {
        self.x * other.x + self.y * other.y
    }

    pub fn lerp(self, other: Vector3, t: f32) -> Vector3 {
        self + (other - self) * t
    }

    pub fn project_down(self) -> Option<Vector> {
        if self.z.is_almost_zero() {
            return None;
        }
        Some(Vector::new(self.x / self.z, self.y / self.z))
    }
}

impl From<[f32; 3]> for Vector3 {
    fn from(array: [f32; 3]) -> Vector3 {
        Vector3::new(array[0], array[1], array[2])
    }
}

impl Into<[f32; 3]> for Vector3 {
    fn into(self) -> [f32; 3] {
        [self.x, self.y, self.z]
    }
}

impl Add for Vector3 {
    type Output = Vector3;

    fn add(self, other: Vector3) -> Vector3 {
        Vector3::new(self.x + other.x, self.y + other.y, self.z + other.z)
    }
}

impl Sub for Vector3 {
    type Output = Vector3;

    fn sub(self, other: Vector3) -> Vector3 {
        Vector3::new(self.x - other.x, self.y - other.y, self.z - other.z)
    }
}

impl Mul<f32> for Vector3 {
    type Output = Vector3;

    fn mul(self, k: f32) -> Vector3 {
        Vector3::new(self.x * k, self.y * k, self.z * k)
    }
}

impl Div<f32> for Vector3 {
    type Output = Vector3;

    fn div(self, k: f32) -> Vector3 {
        Vector3::new(self.x / k, self.y / k, self.z / k)
    }
}

impl Neg for Vector3 {
    type Output = Vector3;

    fn neg(self) -> Vector3 {
        Vector3::new(-self.x, -self.y, -self.z)
    }
}
