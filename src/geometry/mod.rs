mod conic;
mod cubic;
mod f32_ext;
mod line;
mod quadratic;
mod rectangle;
mod vector;
mod vector_3;

pub use self::conic::Conic;
pub use self::cubic::Cubic;
pub use self::f32_ext::F32Ext;
pub use self::line::Line;
pub use self::quadratic::Quadratic;
pub use self::rectangle::Rectangle;
pub use self::vector::Vector;
pub use self::vector_3::Vector3;

use std::f32::consts;
use std::mem;

pub fn solve_linear<F>(a: f32, b: f32, mut f: F) -> bool
where
    F: FnMut(f32) -> bool,
{
    if a.is_almost_zero() {
        if b.is_almost_zero() {
            return f(0.0);
        }
        return true;
    }
    f(-b / a)
}

pub fn solve_quadratic<F>(a: f32, b: f32, c: f32, mut f: F) -> bool
where
    F: FnMut(f32) -> bool,
{
    if a.is_almost_zero() {
        return solve_linear(b, c, f);
    }
    let delta = b * b - 4.0 * a * c;
    if delta < 0.0 {
        return true;
    }
    let q = -(b + b.signum() * delta.sqrt()) / 2.0;
    let mut x0 = q / a;
    let mut x1 = b / q;
    if x0 > x1 {
        mem::swap(&mut x0, &mut x1);
    }
    if !f(x0) {
        return false;
    }
    if !x1.is_almost_equal_to(x0) && !f(x1) {
        return false;
    }
    true
}

pub fn solve_cubic<F>(a: f32, b: f32, c: f32, d: f32, mut f: F) -> bool
where
    F: FnMut(f32) -> bool,
{
    if a.is_almost_zero() {
        return solve_quadratic(b, c, d, f);
    }
    let b = b / a;
    let c = c / a;
    let d = d / a;
    let q = (3.0 * c - b.powi(2)) / 9.0;
    let r = (9.0 * b * c - 27.0 * d - 2.0 * b.powi(3)) / 54.0;
    let delta = q.powi(3) + r.powi(2);
    if delta > 0.0 {
        let s = (r + r.signum() * delta.sqrt()).cbrt();
        let t = -q / s;
        return f(s + t - b / 3.0);
    }
    let theta = (r / (-q.powi(3)).sqrt()).max(-1.0).min(1.0).acos();
    let xs = &mut [
        2.0 * (-q).sqrt() * (theta / 3.0).cos() - b / 3.0,
        2.0 * (-q).sqrt() * ((theta + 2.0 * consts::PI) / 3.0).cos() - b / 3.0,
        2.0 * (-q).sqrt() * ((theta + 4.0 * consts::PI) / 3.0).cos() - b / 3.0,
    ];
    xs.sort_unstable_by(|x0, x1| x0.partial_cmp(x1).unwrap());
    let mut i = 0;
    'outer: while i < xs.len() {
        if !f(xs[i]) {
            return false;
        }
        for j in (i + 1)..xs.len() {
            if !xs[j].is_almost_equal_to(xs[i]) {
                i = j;
                continue 'outer;
            }
        }
        break;
    }
    true
}
