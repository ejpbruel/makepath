use crate::geometry;
use crate::geometry::{Line, Vector};

#[derive(Clone, Copy, Debug)]
pub struct Quadratic {
    pub p0: Vector,
    pub p1: Vector,
    pub p2: Vector,
}

impl Quadratic {
    pub fn new(p0: Vector, p1: Vector, p2: Vector) -> Quadratic {
        Quadratic { p0, p1, p2 }
    }

    pub fn curvature_extrema<F>(self, mut f: F) -> bool
    where
        F: FnMut(f32) -> bool,
    {
        let a = self.p2 - self.p1 * 2.0 + self.p0;
        let b = (self.p1 - self.p0) * 2.0;
        crate::geometry::solve_linear(2.0 * a.dot(a), b.dot(a), |t| {
            t < 0.0 || t > 1.0 || f(t)
        })
    }

    pub fn subdivide_at(self, t: f32) -> (Quadratic, Quadratic) {
        let p01 = self.p0.lerp(self.p1, t);
        let p12 = self.p1.lerp(self.p2, t);
        let p012 = p01.lerp(p12, t);
        (
            Quadratic::new(self.p0, p01, p012),
            Quadratic::new(p012, p12, self.p2),
        )
    }

    pub fn subdivide_at_multiple<F, G>(self, f: F, g: &mut G) -> bool
    where
        F: FnOnce(&mut dyn FnMut(f32) -> bool),
        G: ?Sized + FnMut(Quadratic) -> bool,
    {
        let mut quadratic = self;
        let mut t_start = 0.0;
        f(&mut |t| {
            let s = (t - t_start) / (1.0 - t_start);
            let (quadratic_0, quadratic_1) = quadratic.subdivide_at(s);
            if !g(quadratic_0) {
                return false;
            }
            quadratic = quadratic_1;
            t_start = t;
            true
        });
        g(quadratic)
    }

    pub fn to_lines<F>(self, tolerance: f32, mut f: F) -> bool
    where
        F: FnMut(Line) -> bool,
    {
        self.to_lines_helper(tolerance, &mut f)
    }

    fn to_lines_helper<F>(self, tolerance: f32, f: &mut F) -> bool
    where
        F: FnMut(Line) -> bool,
    {
        let error_sqr = (self.p1 * 2.0 - self.p2 - self.p0).length_sqr();
        if error_sqr <= (4.0 * tolerance).powi(2) {
            return f(Line::new(self.p0, self.p2));
        }
        let (quadratic_0, quadratic_1) = self.subdivide_at(0.5);
        quadratic_0.to_lines_helper(tolerance, f) && quadratic_1.to_lines_helper(tolerance, f)
    }
}
