use crate::geometry::{Rectangle, Vector};
use std::cmp::Ordering;

#[derive(Clone, Copy, Debug)]
pub struct Line {
    pub p0: Vector,
    pub p1: Vector,
}

impl Line {
    pub fn new(p0: Vector, p1: Vector) -> Line {
        Line { p0, p1 }
    }

    pub fn partial_cmp_to_point(self, p: Vector) -> Option<Ordering> {
        (p - self.p0).cross(self.p1 - p).partial_cmp(&0.0)
    }

    pub fn intersect(self, other: Line) -> Option<Vector> {
        let a = self.p1 - self.p0;
        let b = other.p0 - other.p1;
        let c = self.p0 - other.p0;
        let denom = a.cross(b);
        if denom == 0.0 {
            return None;
        }
        let numer_0 = b.cross(c);
        let numer_1 = c.cross(a);
        if denom < 0.0 {
            if (numer_0 < denom || 0.0 < numer_0) || (numer_1 < denom || 0.0 < numer_1) {
                return None;
            }
        } else {
            if (numer_0 < 0.0 || denom < numer_0) || (numer_1 < 0.0 || denom < numer_1) {
                return None;
            }
        }
        Some(self.p0.lerp(self.p1, numer_0 / denom))
    }

    pub fn bounds(self) -> Rectangle {
        Rectangle::new(
            Vector::new(self.p0.x.min(self.p1.x), self.p0.y.min(self.p1.y)),
            Vector::new(self.p0.x.max(self.p1.x), self.p0.y.max(self.p1.y)),
        )
    }
}
