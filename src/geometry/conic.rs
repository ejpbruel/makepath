use crate::geometry::{F32Ext, Line, Quadratic, Vector};

#[derive(Clone, Copy, Debug)]
pub struct Conic {
    pub p0: Vector,
    pub p1: Vector,
    pub p2: Vector,
    pub w: f32,
}

impl Conic {
    pub fn new(p0: Vector, p1: Vector, p2: Vector, w: f32) -> Conic {
        Conic { p0, p1, p2, w }
    }

    pub fn arc<F>(
        center: Vector,
        mut unit_normal_start: Vector,
        unit_normal_end: Vector,
        radius: f32,
        is_ccw: bool,
        mut f: F,
    ) -> bool
    where
        F: FnMut(Conic) -> bool,
    {
        let x = unit_normal_start.dot(unit_normal_end);
        let y = unit_normal_start.cross(unit_normal_end);
        let quadrant;
        if x.is_almost_zero() {
            quadrant = if y < 0.0 { 3 } else { 1 };
        } else if y.is_almost_zero() {
            quadrant = if x < 0.0 { 2 } else { 0 };
        } else if x < 0.0 {
            quadrant = if y < 0.0 { 2 } else { 1 };
        } else {
            quadrant = if y < 0.0 { 3 } else { 0 };
        }
        if is_ccw {
            for _ in 0..quadrant {
                let unit_normal_0 = unit_normal_start;
                let unit_normal_1 = unit_normal_0.perpendicular();
                let ok = f(Conic::new(
                    center + unit_normal_0 * radius,
                    center + (unit_normal_0 + unit_normal_1) * radius,
                    center + unit_normal_1 * radius,
                    0.5 * 2.0f32.sqrt(),
                ));
                if !ok {
                    return false;
                }
                unit_normal_start = unit_normal_1;
            }
        } else {
            for _ in ((quadrant + 1)..4).rev() {
                let unit_normal_0 = unit_normal_start;
                let unit_normal_1 = -unit_normal_0.perpendicular();
                let ok = f(Conic::new(
                    center + unit_normal_0 * radius,
                    center + (unit_normal_0 + unit_normal_1) * radius,
                    center + unit_normal_1 * radius,
                    0.5 * 2.0f32.sqrt(),
                ));
                if !ok {
                    return false;
                }
                unit_normal_start = unit_normal_1;
            }
        }
        let unit_bisector = (unit_normal_start + unit_normal_end).normalize().unwrap();
        let cos_half_angle = ((1.0 + unit_normal_start.dot(unit_normal_end)) / 2.0).sqrt();
        f(Conic::new(
            center + unit_normal_start * radius,
            center + unit_bisector * radius / cos_half_angle,
            center + unit_normal_end * radius,
            cos_half_angle,
        ))
    }

    pub fn subdivide_at(self, t: f32) -> (Conic, Conic) {
        let p0 = self.p0.project_up(1.0);
        let p1 = self.p1.project_up(self.w);
        let p2 = self.p2.project_up(1.0);
        let p01 = p0.lerp(p1, t);
        let p12 = p1.lerp(p2, t);
        let p012 = p01.lerp(p12, t);
        (
            Conic::new(
                self.p0,
                p01.project_down().unwrap(),
                p012.project_down().unwrap(),
                p01.z / p012.z.sqrt(),
            ),
            Conic::new(
                p012.project_down().unwrap(),
                p12.project_down().unwrap(),
                self.p2,
                p12.z / p012.z.sqrt(),
            ),
        )
    }

    pub fn to_lines<F>(self, tolerance: f32, mut f: F) -> bool
    where
        F: FnMut(Line) -> bool,
    {
        self.to_lines_helper(tolerance, &mut f)
    }

    pub fn to_quadratics<F>(self, tolerance: f32, mut f: F) -> bool
    where
        F: FnMut(Quadratic) -> bool,
    {
        self.to_quadratics_helper(tolerance, &mut f)
    }

    fn to_lines_helper<F>(self, tolerance: f32, f: &mut F) -> bool
    where
        F: FnMut(Line) -> bool,
    {
        let error_sqr = (self.p1 * 2.0 - self.p2 - self.p0).length_sqr();
        if error_sqr <= (2.0 * (1.0 + self.w) * tolerance).powi(2) {
            return f(Line::new(self.p0, self.p2));
        }
        let (conic_0, conic_1) = self.subdivide_at(0.5);
        conic_0.to_lines_helper(tolerance, f) && conic_1.to_lines_helper(tolerance, f)
    }

    fn to_quadratics_helper<F>(self, tolerance: f32, f: &mut F) -> bool
    where
        F: FnMut(Quadratic) -> bool,
    {
        let a = self.w - 1.0;
        let error_sqr = a.powi(2) * (self.p0 - self.p1 * 2.0 + self.p2).length_sqr();
        if error_sqr <= (4.0 * (2.0 + a) * tolerance).powi(2) {
            return f(Quadratic::new(self.p0, self.p1, self.p2));
        }
        let (conic_0, conic_1) = self.subdivide_at(0.5);
        conic_0.to_quadratics_helper(tolerance, f) && conic_1.to_quadratics_helper(tolerance, f)
    }
}
