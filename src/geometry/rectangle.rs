use crate::geometry::Vector;

#[derive(Clone, Copy, Debug)]
pub struct Rectangle {
    pub min: Vector,
    pub max: Vector,
}

impl Rectangle {
    pub fn new(min: Vector, max: Vector) -> Rectangle {
        Rectangle { min, max }
    }

    pub fn intersect(self, other: Rectangle) -> Rectangle {
        Rectangle::new(
            Vector::new(self.min.x.max(other.min.x), self.min.y.max(other.min.y)),
            Vector::new(self.max.x.min(other.max.x), self.max.y.min(other.max.y)),
        )
    }
}
