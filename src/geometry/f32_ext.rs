pub trait F32Ext {
    fn is_almost_zero(self) -> bool;

    fn is_almost_equal_to(self, other: f32) -> bool;
}

impl F32Ext for f32 {
    fn is_almost_zero(self) -> bool {
        self.abs() <= 1.0E-3
    }

    fn is_almost_equal_to(self, other: f32) -> bool {
        (other - self).is_almost_zero()
    }
}
