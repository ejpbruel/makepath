use crate::geometry;
use crate::geometry::{Line, Vector};

#[derive(Clone, Copy, Debug)]
pub struct Cubic {
    pub p0: Vector,
    pub p1: Vector,
    pub p2: Vector,
    pub p3: Vector,
}

impl Cubic {
    pub fn new(p0: Vector, p1: Vector, p2: Vector, p3: Vector) -> Cubic {
        Cubic { p0, p1, p2, p3 }
    }

    pub fn is_almost_flat(self, tolerance: f32) -> bool {
        let u = self.p1 * 3.0 - self.p0 * 2.0 - self.p3;
        let v = self.p2 * 3.0 - self.p3 * 2.0 - self.p0;
        (u.x.powi(2).max(v.x.powi(2)) + u.y.powi(2).max(v.y.powi(2)))
            <= 16.0 * tolerance * tolerance
    }

    pub fn curvature_extrema<F>(self, mut f: F) -> bool
    where
        F: FnMut(f32) -> bool,
    {
        let a = self.p3 - (self.p2 - self.p1) * 3.0 - self.p0;
        let b = self.p2 - self.p1 * 2.0 + self.p0;
        let c = self.p1 - self.p0;
        crate::geometry::solve_cubic(
            a.dot(a),
            3.0 * a.dot(b),
            a.dot(c) + 2.0 * b.dot(b),
            b.dot(c),
            |t| t < 0.0 || t > 1.0 || f(t),
        )
    }

    pub fn subdivide_at(self, t: f32) -> (Cubic, Cubic) {
        let p01 = self.p0.lerp(self.p1, t);
        let p12 = self.p1.lerp(self.p2, t);
        let p23 = self.p2.lerp(self.p3, t);
        let p012 = p01.lerp(p12, t);
        let p123 = p12.lerp(p23, t);
        let p0123 = p012.lerp(p123, t);
        (
            Cubic::new(self.p0, p01, p012, p0123),
            Cubic::new(p0123, p123, p23, self.p3),
        )
    }

    pub fn subdivide_at_multiple<F, G>(self, f: F, g: &mut G) -> bool
    where
        F: FnOnce(&mut dyn FnMut(f32) -> bool),
        G: ?Sized + FnMut(Cubic) -> bool,
    {
        let mut cubic = self;
        let mut t_start = 0.0;
        f(&mut |t| {
            let s = (t - t_start) / (1.0 - t_start);
            let (cubic_0, cubic_1) = cubic.subdivide_at(s);
            if !g(cubic_0) {
                return false;
            }
            cubic = cubic_1;
            t_start = t;
            true
        });
        g(cubic)
    }

    pub fn to_lines<F>(self, tolerance: f32, mut f: F) -> bool
    where
        F: FnMut(Line) -> bool,
    {
        self.to_lines_helper(tolerance, &mut f)
    }

    fn to_lines_helper<F>(self, tolerance: f32, f: &mut F) -> bool
    where
        F: FnMut(Line) -> bool,
    {
        let u = self.p1 * 3.0 - self.p0 * 2.0 - self.p3;
        let v = self.p2 * 3.0 - self.p3 * 2.0 - self.p0;
        let error_sqr = u.x.powi(2).max(v.x.powi(2)) + u.y.powi(2).max(v.y.powi(2));
        if error_sqr <= (4.0 * tolerance).powi(2) {
            return f(Line::new(self.p0, self.p3));
        }
        let (cubic_0, cubic_1) = self.subdivide_at(0.5);
        cubic_0.to_lines_helper(tolerance, f) && cubic_1.to_lines_helper(tolerance, f)
    }
}
