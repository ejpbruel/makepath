use crate::geometry::{Conic, Cubic, F32Ext, Line, Quadratic, Vector};
use crate::path::Command;
use crate::Path;

#[derive(Clone, Debug)]
pub struct Stroker {
    pub radius: f32,
    pub cap: Cap,
    pub join: Join,
    initial_point: Option<Vector>,
    initial_unit_normal: Option<Vector>,
    current_point: Option<Vector>,
    current_unit_normal: Option<Vector>,
    left_offset: Path,
}

impl Stroker {
    pub fn new(options: Options) -> Stroker {
        Stroker {
            radius: options.radius,
            cap: options.cap,
            join: options.join,
            initial_point: None,
            initial_unit_normal: None,
            current_point: None,
            current_unit_normal: None,
            left_offset: Path::new(),
        }
    }

    pub fn stroke<C, F>(&mut self, commands: C, mut f: F) -> bool
    where
        C: FnOnce(&mut dyn FnMut(Command) -> bool),
        F: FnMut(Command) -> bool,
    {
        commands(&mut |command| match command {
            Command::MoveTo(p) => self.move_to(p, &mut f),
            Command::LineTo(p1) => self.line_to(p1, &mut f),
            Command::QuadraticTo(p1, p2) => self.quadratic_to(p1, p2, &mut f),
            Command::ConicTo(p1, p2, w) => self.conic_to(p1, p2, w, &mut f),
            Command::CubicTo(p1, p2, p3) => self.cubic_to(p1, p2, p3, &mut f),
            Command::Close => self.close(&mut f),
        });
        let ok = (|| {
            if self.initial_unit_normal.is_some() {
                let ok = self.finish_contour(false, f);
                if !ok {
                    return false;
                }
            }
            true
        })();
        self.left_offset.clear();
        ok
    }

    fn move_to<F>(&mut self, p: Vector, f: F) -> bool
    where
        F: FnMut(Command) -> bool,
    {
        if self.initial_unit_normal.is_some() {
            let ok = self.finish_contour(false, f);
            if !ok {
                return false;
            }
        }
        self.initial_point = Some(p);
        self.current_point = Some(p);
        true
    }

    fn line_to<F>(&mut self, p1: Vector, mut f: F) -> bool
    where
        F: FnMut(Command) -> bool,
    {
        let line = Line::new(self.current_point.unwrap(), p1);
        let tangent_p0p1 = line.p1 - line.p0;
        if tangent_p0p1.is_almost_zero() {
            return true;
        }
        let unit_normal_p0p1 = tangent_p0p1.perpendicular().normalize().unwrap();
        let ok = self.start_segment(unit_normal_p0p1, &mut f)
            && f(Command::LineTo(line.p1 - unit_normal_p0p1 * self.radius));
        if !ok {
            return false;
        };
        self.left_offset
            .line_to(line.p1 + unit_normal_p0p1 * self.radius);
        self.finish_segment(line.p1, unit_normal_p0p1);
        true
    }

    fn quadratic_to<F>(&mut self, p1: Vector, p2: Vector, mut f: F) -> bool
    where
        F: FnMut(Command) -> bool,
    {
        let quadratic = Quadratic::new(self.current_point.unwrap(), p1, p2);
        quadratic.subdivide_at_multiple(
            |f| {
                quadratic.curvature_extrema(f);
            },
            &mut |quadratic| self.quadratic_to_helper(quadratic, 8, &mut f),
        )
    }

    fn quadratic_to_helper<F>(
        &mut self,
        quadratic: Quadratic,
        max_subdivide: u32,
        mut f: &mut F,
    ) -> bool
    where
        F: FnMut(Command) -> bool,
    {
        let tangent_p0p1 = quadratic.p1 - quadratic.p0;
        let tangent_p1p2 = quadratic.p2 - quadratic.p1;
        if tangent_p0p1.is_almost_zero() || tangent_p1p2.is_almost_zero() {
            return self.line_to(quadratic.p2, f);
        }
        let unit_normal_p0p1 = tangent_p0p1.perpendicular().normalize().unwrap();
        let unit_normal_p1p2 = tangent_p1p2.perpendicular().normalize().unwrap();
        let cos_angle_p0p1p2 = unit_normal_p0p1.dot(unit_normal_p1p2);
        if cos_angle_p0p1p2 <= 0.75 {
            if max_subdivide == 0 {
                return self.line_to(quadratic.p2, f);
            }
            let (quadratic_0, quadratic_1) = quadratic.subdivide_at(0.5);
            return self.quadratic_to_helper(quadratic_0, max_subdivide - 1, f)
                && self.quadratic_to_helper(quadratic_1, max_subdivide - 1, f);
        }
        let unit_normal_p1 = (unit_normal_p0p1 + unit_normal_p1p2).normalize().unwrap();
        let d1 = self.radius / ((1.0 + cos_angle_p0p1p2) / 2.0).sqrt();
        let ok = self.start_segment(unit_normal_p0p1, &mut f)
            && f(Command::QuadraticTo(
                quadratic.p1 - unit_normal_p1 * d1,
                quadratic.p2 - unit_normal_p1p2 * self.radius,
            ));
        if !ok {
            return false;
        }
        self.left_offset.quadratic_to(
            quadratic.p1 + unit_normal_p1 * d1,
            quadratic.p2 + unit_normal_p1p2 * self.radius,
        );
        self.finish_segment(quadratic.p2, unit_normal_p1p2);
        true
    }

    fn conic_to<F>(&mut self, p1: Vector, p2: Vector, w: f32, mut f: F) -> bool
    where
        F: FnMut(Command) -> bool,
    {
        Conic::new(self.current_point.unwrap(), p1, p2, w).to_quadratics(1E-3, |quadratic| {
            self.quadratic_to_helper(quadratic, 8, &mut f)
        })
    }

    fn cubic_to<F>(&mut self, p1: Vector, p2: Vector, p3: Vector, mut f: F) -> bool
    where
        F: FnMut(Command) -> bool,
    {
        let cubic = Cubic::new(self.current_point.unwrap(), p1, p2, p3);
        cubic.subdivide_at_multiple(
            |f| {
                cubic.curvature_extrema(f);
            },
            &mut |cubic| self.cubic_to_helper(cubic, 8, &mut f),
        )
    }

    fn cubic_to_helper<F>(&mut self, cubic: Cubic, max_subdivide: u32, mut f: &mut F) -> bool
    where
        F: FnMut(Command) -> bool,
    {
        let tangent_p1p2 = cubic.p2 - cubic.p1;
        let mut tangent_p0p1 = cubic.p1 - cubic.p0;
        if tangent_p0p1.is_almost_zero() {
            if tangent_p1p2.is_almost_zero() {
                return self.line_to(cubic.p3, f);
            }
            tangent_p0p1 = tangent_p1p2;
        }
        let unit_normal_p0p1 = tangent_p0p1.perpendicular().normalize().unwrap();
        let mut tangent_p2p3 = cubic.p3 - cubic.p2;
        if tangent_p2p3.is_almost_zero() {
            if tangent_p1p2.is_almost_zero() {
                return self.line_to(cubic.p3, f);
            }
            tangent_p2p3 = tangent_p1p2;
        }
        let unit_normal_p2p3 = tangent_p2p3.perpendicular().normalize().unwrap();
        let unit_normal_p1;
        let d1;
        let unit_normal_p2;
        let d2;
        if tangent_p1p2.is_almost_zero() {
            let cos_theta_p0p1p3 = unit_normal_p0p1.dot(unit_normal_p2p3);
            if cos_theta_p0p1p3 <= 0.75 {
                if max_subdivide == 0 {
                    return self.line_to(cubic.p3, f);
                }
                let (cubic_0, cubic_1) = cubic.subdivide_at(0.5);
                return self.cubic_to_helper(cubic_0, max_subdivide - 1, f)
                    && self.cubic_to_helper(cubic_1, max_subdivide - 1, f);
            }
            unit_normal_p1 = (unit_normal_p0p1 + unit_normal_p2p3).normalize().unwrap();
            d1 = self.radius / ((1.0 + cos_theta_p0p1p3) / 2.0).sqrt();
            unit_normal_p2 = unit_normal_p1;
            d2 = d1;
        } else {
            let unit_normal_p1p2 = tangent_p1p2.perpendicular().normalize().unwrap();
            let cos_angle_p0p1p2 = unit_normal_p0p1.dot(unit_normal_p1p2);
            let cos_angle_p1p2p3 = unit_normal_p1p2.dot(unit_normal_p2p3);
            if cos_angle_p0p1p2 <= 0.75 || cos_angle_p1p2p3 <= 0.75 {
                if max_subdivide == 0 {
                    return self.line_to(cubic.p3, f);
                }
                let (cubic_0, cubic_1) = cubic.subdivide_at(0.5);
                return self.cubic_to_helper(cubic_0, max_subdivide - 1, f)
                    && self.cubic_to_helper(cubic_1, max_subdivide - 1, f);
            }
            unit_normal_p1 = (unit_normal_p0p1 + unit_normal_p1p2).normalize().unwrap();
            d1 = self.radius / ((1.0 + cos_angle_p0p1p2) / 2.0).sqrt();
            unit_normal_p2 = (unit_normal_p1p2 + unit_normal_p2p3).normalize().unwrap();
            d2 = self.radius / ((1.0 + cos_angle_p1p2p3) / 2.0).sqrt();
        }
        let ok = self.start_segment(unit_normal_p0p1, &mut f)
            && f(Command::CubicTo(
                cubic.p1 - unit_normal_p1 * d1,
                cubic.p2 - unit_normal_p2 * d2,
                cubic.p3 - unit_normal_p2p3 * self.radius,
            ));
        if !ok {
            return false;
        }
        self.left_offset.cubic_to(
            cubic.p1 + unit_normal_p1 * d1,
            cubic.p2 + unit_normal_p2 * d2,
            cubic.p3 + unit_normal_p2p3 * self.radius,
        );
        self.finish_segment(cubic.p3, unit_normal_p2p3);
        true
    }

    fn close<F>(&mut self, mut f: F) -> bool
    where
        F: FnMut(Command) -> bool,
    {
        let ok = self.line_to(self.initial_point.unwrap(), &mut f);
        if !ok {
            return false;
        }
        if self.initial_unit_normal.is_some() {
            let ok = self.finish_contour(true, f);
            if !ok {
                return false;
            }
        }
        self.current_point = Some(self.initial_point.unwrap());
        true
    }

    fn finish_contour<F>(&mut self, is_closed: bool, mut f: F) -> bool
    where
        F: FnMut(Command) -> bool,
    {
        if is_closed {
            let ok = self.join(self.initial_unit_normal.unwrap(), &mut f)
                && f(Command::Close)
                && self.left_offset.commands_backwards(&mut f)
                && f(Command::Close);
            if !ok {
                return false;
            }
        } else {
            let ok = self.cap(&mut f);
            if !ok {
                return false;
            }
            let mut is_first = true;
            let ok = self.left_offset.commands_backwards(|command| {
                if is_first {
                    is_first = false;
                    return true;
                }
                f(command)
            });
            if !ok {
                return false;
            }
            self.current_point = Some(self.initial_point.unwrap());
            self.current_unit_normal = Some(-self.initial_unit_normal.unwrap());
            let ok = self.cap(&mut f) && f(Command::Close);
            if !ok {
                return false;
            }
        }
        self.initial_unit_normal = None;
        self.current_unit_normal = None;
        true
    }

    fn start_segment<F>(&mut self, unit_normal_start: Vector, mut f: F) -> bool
    where
        F: FnMut(Command) -> bool,
    {
        if self.initial_unit_normal.is_none() {
            let p_start = self.current_point.unwrap();
            let ok = f(Command::MoveTo(p_start - unit_normal_start * self.radius));
            if !ok {
                return false;
            }
            self.left_offset
                .move_to(p_start + unit_normal_start * self.radius);
            self.initial_unit_normal = Some(unit_normal_start);
            return true;
        }
        self.join(unit_normal_start, f)
    }

    fn finish_segment(&mut self, p_end: Vector, unit_normal_end: Vector) {
        self.current_point = Some(p_end);
        self.current_unit_normal = Some(unit_normal_end);
    }

    fn cap<F>(&mut self, f: F) -> bool
    where
        F: FnMut(Command) -> bool,
    {
        let pivot = self.current_point.unwrap();
        let unit_normal_start = self.current_unit_normal.unwrap();
        (match self.cap {
            Cap::Butt => butt_cap,
            Cap::Round => round_cap,
            Cap::Square => square_cap,
        })(pivot, unit_normal_start, self.radius, f)
    }

    fn join<F>(&mut self, unit_normal_end: Vector, f: F) -> bool
    where
        F: FnMut(Command) -> bool,
    {
        let pivot = self.current_point.unwrap();
        let unit_normal_start = self.current_unit_normal.unwrap();
        match self.join {
            Join::Bevel => bevel_join(
                pivot,
                unit_normal_start,
                unit_normal_end,
                self.radius,
                &mut self.left_offset,
                f,
            ),
            Join::Miter { limit } => miter_join(
                pivot,
                unit_normal_start,
                unit_normal_end,
                self.radius,
                limit,
                &mut self.left_offset,
                f,
            ),
            Join::Round => round_join(
                pivot,
                unit_normal_start,
                unit_normal_end,
                self.radius,
                &mut self.left_offset,
                f,
            ),
        }
    }
}

#[derive(Clone, Copy, Debug)]
pub struct Options {
    pub radius: f32,
    pub cap: Cap,
    pub join: Join,
}

impl Default for Options {
    fn default() -> Options {
        Options {
            radius: 0.5,
            cap: Cap::default(),
            join: Join::default(),
        }
    }
}

#[derive(Clone, Copy, Debug)]
pub enum Cap {
    Butt,
    Round,
    Square,
}

impl Default for Cap {
    fn default() -> Cap {
        Cap::Butt
    }
}

#[derive(Clone, Copy, Debug)]
pub enum Join {
    Bevel,
    Miter { limit: f32 },
    Round,
}

impl Default for Join {
    fn default() -> Join {
        Join::Miter { limit: 4.0 }
    }
}

fn butt_cap<F>(pivot: Vector, unit_normal_start: Vector, radius: f32, mut f: F) -> bool
where
    F:  FnMut(Command) -> bool,
{
    f(Command::LineTo(pivot + unit_normal_start * radius))
}

fn round_cap<F>(pivot: Vector, unit_normal_start: Vector, radius: f32, mut f: F) -> bool
where
    F:  FnMut(Command) -> bool,
{
    Conic::arc(
        pivot,
        -unit_normal_start,
        unit_normal_start,
        radius,
        true,
        |conic| f(Command::ConicTo(conic.p1, conic.p2, conic.w)),
    )
}

fn square_cap<F>(pivot: Vector, unit_normal_start: Vector, radius: f32, mut f: F) -> bool
where
    F:  FnMut(Command) -> bool,
{
    let unit_bisector = unit_normal_start.perpendicular();
    f(Command::LineTo(
        pivot - (unit_normal_start + unit_bisector) * radius,
    )) && f(Command::LineTo(
        pivot + (unit_normal_start - unit_bisector) * radius,
    )) && f(Command::LineTo(pivot + unit_normal_start * radius))
}

fn bevel_join<F>(
    pivot: Vector,
    unit_normal_start: Vector,
    unit_normal_end: Vector,
    radius: f32,
    left_offset: &mut Path,
    mut f: F,
) -> bool
where
    F:  FnMut(Command) -> bool,
{
    let sin_angle = unit_normal_start.cross(unit_normal_end);
    if sin_angle.is_almost_zero() {
        return true;
    }
    let ok = f(Command::LineTo(pivot - unit_normal_end * radius));
    if !ok {
        return false;
    }
    left_offset.push(Command::LineTo(pivot + unit_normal_end * radius));
    true
}

fn miter_join<F>(
    pivot: Vector,
    unit_normal_start: Vector,
    unit_normal_end: Vector,
    radius: f32,
    limit: f32,
    left_offset: &mut Path,
    mut f: F,
) -> bool
where
    F:  FnMut(Command) -> bool,
{
    let sin_angle = unit_normal_start.cross(unit_normal_end);
    if sin_angle.is_almost_zero() {
        return true;
    }
    let cos_half_angle = ((1.0 + unit_normal_start.dot(unit_normal_end)) / 2.0).sqrt();
    let unit_bisector = (unit_normal_start + unit_normal_end).normalize().unwrap();
    if sin_angle < 0.0 {
        let ok = f(Command::LineTo(pivot - unit_normal_end * radius));
        if !ok {
            return false;
        }
        if cos_half_angle * limit >= 1.0 {
            left_offset.push(Command::LineTo(
                pivot + unit_bisector * (radius / cos_half_angle),
            ));
        }
        left_offset.push(Command::LineTo(pivot + unit_normal_end * radius));
    } else {
        if cos_half_angle * limit >= 1.0 {
            let ok = f(Command::LineTo(
                pivot - unit_bisector * (radius / cos_half_angle),
            ));
            if !ok {
                return false;
            }
        }
        let ok = f(Command::LineTo(pivot - unit_normal_end * radius));
        if !ok {
            return false;
        }
        left_offset.push(Command::LineTo(pivot + unit_normal_end * radius));
    }
    true
}

fn round_join<F>(
    pivot: Vector,
    unit_normal_start: Vector,
    unit_normal_end: Vector,
    radius: f32,
    left_offset: &mut Path,
    mut f: F,
) -> bool
where
    F:  FnMut(Command) -> bool,
{
    let sin_angle = unit_normal_start.cross(unit_normal_end);
    if sin_angle.is_almost_zero() {
        return true;
    }
    if sin_angle < 0.0 {
        let ok = f(Command::LineTo(pivot - unit_normal_end * radius));
        if !ok {
            return false;
        }
        Conic::arc(
            pivot,
            unit_normal_start,
            unit_normal_end,
            radius,
            false,
            |conic| {
                left_offset.push(Command::ConicTo(conic.p1, conic.p2, conic.w));
                true
            },
        );
    } else {
        let ok = Conic::arc(
            pivot,
            -unit_normal_start,
            -unit_normal_end,
            radius,
            true,
            |conic| f(Command::ConicTo(conic.p1, conic.p2, conic.w)),
        );
        if !ok {
            return false;
        }
        left_offset.push(Command::LineTo(pivot + unit_normal_end * radius));
    }
    true
}

#[cfg(test)]
mod tests;
