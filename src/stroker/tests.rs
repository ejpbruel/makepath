use crate::geometry::Vector;
use crate::stroker::{Cap, Join, Options};
use crate::{Path, Stroker};

#[test]
fn test_line() {
    let mut stroker = Stroker::new(Options {
        radius: 1.0,
        cap: Cap::Butt,
        join: Join::Bevel
    });
    let mut path = Path::new();
    path.move_to(Vector::new(-10.0, 0.0));
    path.line_to(Vector::new(10.0, 0.0));
    let actual = Path::from_commands(|f| {
        stroker.stroke(
            |g| {
                path.commands(g);
            },
            f,
        );
    });
    let mut expected = Path::new();
    expected.move_to(Vector::new(-10.0, -1.0));
    expected.line_to(Vector::new(10.0, -1.0));
    expected.line_to(Vector::new(10.0, 1.0));
    expected.line_to(Vector::new(-10.0, 1.0));
    expected.line_to(Vector::new(-10.0, -1.0));
    expected.close();
    assert_eq!(actual, expected);
}

#[test]
fn test_closed_path() {
    let mut stroker = Stroker::new(Options {
        radius: 1.0,
        cap: Cap::Butt,
        join: Join::Bevel
    });
    let mut path = Path::new();
    path.move_to(Vector::new(-10.0, -10.0));
    path.line_to(Vector::new(10.0, -10.0));
    path.line_to(Vector::new(10.0, 10.0));
    path.line_to(Vector::new(-10.0, 10.0));
    path.close();
    let actual = Path::from_commands(|f| {
        stroker.stroke(
            |g| {
                path.commands(g);
            },
            f,
        );
    });
    let mut expected = Path::new();
    expected.move_to(Vector::new(-10.0, -11.0));
    expected.line_to(Vector::new(10.0, -11.0));
    expected.line_to(Vector::new(11.0, -10.0));
    expected.line_to(Vector::new(11.0, 10.0));
    expected.line_to(Vector::new(10.0, 11.0));
    expected.line_to(Vector::new(-10.0, 11.0));
    expected.line_to(Vector::new(-11.0, 10.0));
    expected.line_to(Vector::new(-11.0, -10.0));
    expected.line_to(Vector::new(-10.0, -11.0));
    expected.close();
    expected.move_to(Vector::new(-10.0, -9.0));
    expected.line_to(Vector::new(-9.0, -10.0));
    expected.line_to(Vector::new(-9.0, 10.0));
    expected.line_to(Vector::new(-10.0, 9.0));
    expected.line_to(Vector::new(10.0, 9.0));
    expected.line_to(Vector::new(9.0, 10.0));
    expected.line_to(Vector::new(9.0, -10.0));
    expected.line_to(Vector::new(10.0, -9.0));
    expected.line_to(Vector::new(-10.0, -9.0));
    expected.close();
    assert_eq!(actual, expected);
}