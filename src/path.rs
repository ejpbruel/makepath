use crate::geometry::Vector;

#[derive(Clone, Debug, Default, PartialEq)]
pub struct Path {
    verbs: Vec<Verb>,
    points: Vec<Vector>,
    weights: Vec<f32>,
}

impl Path {
    pub fn new() -> Path {
        Path {
            verbs: Vec::new(),
            points: Vec::new(),
            weights: Vec::new(),
        }
    }

    pub fn from_commands<F>(f: F) -> Path
    where
        F: FnOnce(&mut dyn FnMut(Command) -> bool),
    {
        let mut path = Path::new();
        path.extend_from_commands(f);
        path
    }

    pub fn points(&self) -> &[Vector] {
        &self.points
    }

    pub fn commands<F>(&self, mut f: F) -> bool
    where
        F: FnMut(Command) -> bool,
    {
        let mut points = &self.points[..];
        let mut weights = self.weights.iter().cloned();
        for verb in &self.verbs {
            let ok = match verb {
                Verb::MoveTo => {
                    let p = points[0];
                    points = &points[1..];
                    f(Command::MoveTo(p))
                }
                Verb::LineTo => {
                    let p1 = points[0];
                    points = &points[1..];
                    f(Command::LineTo(p1))
                }
                Verb::QuadraticTo => {
                    let ps = &points[..2];
                    points = &points[2..];
                    f(Command::QuadraticTo(ps[0], ps[1]))
                }
                Verb::ConicTo => {
                    let ps = &points[..2];
                    points = &points[2..];
                    f(Command::ConicTo(ps[0], ps[1], weights.next().unwrap()))
                }
                Verb::CubicTo => {
                    let ps = &points[..3];
                    points = &points[3..];
                    f(Command::CubicTo(ps[0], ps[1], ps[2]))
                }
                Verb::Close => f(Command::Close),
            };
            if !ok {
                return false;
            }
        }
        true
    }

    pub fn commands_backwards<F>(&self, mut f: F) -> bool
    where
        F: FnMut(Command) -> bool,
    {
        let mut points = self.points.as_slice();
        let mut weights = self.weights.iter().cloned().rev();
        let mut is_closed = false;
        let (&p0, points_0) = points.split_last().unwrap();
        points = points_0;
        let ok = f(Command::MoveTo(p0));
        if !ok {
            return false;
        }
        for &verb in self.verbs[1..].iter().rev() {
            let ok = match verb {
                Verb::MoveTo => {
                    if is_closed {
                        let ok = f(Command::Close);
                        if !ok {
                            return false;
                        }
                    }
                    is_closed = false;
                    let p = points[points.len() - 1];
                    points = &points[..(points.len() - 1)];
                    f(Command::MoveTo(p))
                }
                Verb::LineTo => {
                    let p1 = points[points.len() - 1];
                    points = &points[..(points.len() - 1)];
                    f(Command::LineTo(p1))
                }
                Verb::QuadraticTo => {
                    let ps = &points[(points.len() - 2)..];
                    points = &points[..(points.len() - 2)];
                    f(Command::QuadraticTo(ps[1], ps[0]))
                }
                Verb::ConicTo => {
                    let ps = &points[(points.len() - 2)..];
                    points = &points[..(points.len() - 2)];
                    f(Command::ConicTo(ps[1], ps[0], weights.next().unwrap()))
                }
                Verb::CubicTo => {
                    let ps = &points[(points.len() - 3)..];
                    points = &points[..(points.len() - 3)];
                    f(Command::CubicTo(ps[2], ps[1], ps[0]))
                }
                Verb::Close => {
                    if is_closed {
                        let ok = f(Command::Close);
                        if !ok {
                            return false;
                        }
                    }
                    is_closed = true;
                    let (&p0, points_0) = points.split_last().unwrap();
                    points = points_0;
                    f(Command::MoveTo(p0))
                }
            };
            if !ok {
                return false;
            }
        }
        if is_closed {
            let ok = f(Command::Close);
            if !ok {
                return false;
            }
        }
        true
    }

    pub fn points_mut(&mut self) -> &mut [Vector] {
        &mut self.points
    }

    pub fn push(&mut self, command: Command) {
        match command {
            Command::MoveTo(p) => self.move_to(p),
            Command::LineTo(p1) => self.line_to(p1),
            Command::QuadraticTo(p1, p2) => self.quadratic_to(p1, p2),
            Command::ConicTo(p1, p2, w) => self.conic_to(p1, p2, w),
            Command::CubicTo(p1, p2, p3) => self.cubic_to(p1, p2, p3),
            Command::Close => self.close(),
        }
    }

    pub fn move_to(&mut self, p: Vector) {
        self.verbs.push(Verb::MoveTo);
        self.points.push(p);
    }

    pub fn line_to(&mut self, p1: Vector) {
        self.verbs.push(Verb::LineTo);
        self.points.push(p1);
    }

    pub fn quadratic_to(&mut self, p1: Vector, p2: Vector) {
        self.verbs.push(Verb::QuadraticTo);
        self.points.extend(&[p1, p2]);
    }

    pub fn conic_to(&mut self, p1: Vector, p2: Vector, w: f32) {
        self.verbs.push(Verb::ConicTo);
        self.points.extend(&[p1, p2]);
        self.weights.push(w);
    }

    pub fn cubic_to(&mut self, p1: Vector, p2: Vector, p3: Vector) {
        self.verbs.push(Verb::CubicTo);
        self.points.extend(&[p1, p2, p3]);
    }

    pub fn close(&mut self) {
        self.verbs.push(Verb::Close);
    }

    pub fn extend_from_commands<F>(&mut self, f: F)
    where
        F: FnOnce(&mut dyn FnMut(Command) -> bool),
    {
        f(&mut |command| {
            self.push(command);
            true
        });
    }

    pub fn clear(&mut self) {
        self.verbs.clear();
        self.points.clear();
        self.weights.clear();
    }
}

#[derive(Clone, Copy, Debug)]
pub enum Command {
    MoveTo(Vector),
    LineTo(Vector),
    QuadraticTo(Vector, Vector),
    ConicTo(Vector, Vector, f32),
    CubicTo(Vector, Vector, Vector),
    Close,
}

#[derive(Clone, Copy, Debug, PartialEq)]
enum Verb {
    MoveTo,
    LineTo,
    QuadraticTo,
    ConicTo,
    CubicTo,
    Close,
}
