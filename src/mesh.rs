use crate::geometry::Vector;

#[derive(Clone, Debug, Default, PartialEq)]
pub struct Mesh {
    pub vertices: Vec<Vertex>,
    pub indices: Vec<u16>,
}

impl Mesh {
    pub fn new() -> Mesh {
        Mesh {
            vertices: Vec::new(),
            indices: Vec::new(),
        }
    }

    pub fn with_vertices_and_indices(vertices: Vec<Vertex>, indices: Vec<u16>) -> Mesh {
        Mesh { vertices, indices }
    }
}

#[derive(Clone, Copy, Debug, PartialEq)]
#[repr(C)]
pub struct Vertex {
    pub position: [f32; 2],
}

impl Vertex {
    pub fn new(position: Vector) -> Vertex {
        Vertex {
            position: position.into(),
        }
    }
}
