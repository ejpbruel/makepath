use gl::types::{GLenum, GLsizei, GLsizeiptr, GLuint};
use glutin::dpi::LogicalSize;
use glutin::event::{Event, WindowEvent};
use glutin::event_loop::{ControlFlow, EventLoop};
use glutin::window::{Window, WindowBuilder};
use glutin::{ContextBuilder, ContextWrapper, PossiblyCurrent};
use makepath::geometry::{Conic, Cubic, Quadratic};
use makepath::path::Command;
use makepath::{Mesh, Path};
use std::ffi::{c_void, CStr, CString};
use std::mem;
use std::ptr;

pub trait Draw {
    fn draw(&self, builder: &mut PrimitiveBuilder, options: DrawOptions);
}

impl Draw for Mesh {
    fn draw(&self, builder: &mut PrimitiveBuilder, options: DrawOptions) {
        builder.begin(gl::TRIANGLES);
        builder.color(options.face_color);
        for triangle in self.indices.chunks(3) {
            for &index in triangle {
                builder.vertex(self.vertices[index as usize].position);
            }
        }
        builder.end();
        builder.begin(gl::LINES);
        builder.color(options.edge_color);
        for triangle in self.indices.chunks(3) {
            let position_0 = self.vertices[triangle[0] as usize].position;
            let position_1 = self.vertices[triangle[1] as usize].position;
            let position_2 = self.vertices[triangle[2] as usize].position;
            builder.vertex(position_0);
            builder.vertex(position_1);
            builder.vertex(position_1);
            builder.vertex(position_2);
            builder.vertex(position_2);
            builder.vertex(position_0);
        }
        builder.end();
        builder.begin(gl::POINTS);
        builder.color(options.vertex_color);
        for &index in &self.indices {
            builder.vertex(self.vertices[index as usize].position);
        }
        builder.end();
    }
}

impl Draw for Path {
    fn draw(&self, builder: &mut PrimitiveBuilder, options: DrawOptions) {
        builder.begin(gl::LINES);
        builder.color(options.edge_color);
        let mut initial_vertex = None;
        let mut current_vertex = None;
        self.commands(|command| {
            match command {
                Command::MoveTo(p) => {
                    initial_vertex = Some(p);
                    current_vertex = Some(p);
                }
                Command::LineTo(p1) => {
                    let p0 = current_vertex.unwrap();
                    builder.vertex(p0.into());
                    builder.vertex(p1.into());
                    current_vertex = Some(p1);
                }
                Command::QuadraticTo(p1, p2) => {
                    Quadratic::new(current_vertex.unwrap(), p1, p2).to_lines(1E-3, |edge| {
                        builder.vertex(edge.p0.into());
                        builder.vertex(edge.p1.into());
                        true
                    });
                    current_vertex = Some(p2);
                }
                Command::ConicTo(p1, p2, w) => {
                    Conic::new(current_vertex.unwrap(), p1, p2, w).to_lines(1E-3, |edge| {
                        builder.vertex(edge.p0.into());
                        builder.vertex(edge.p1.into());
                        true
                    });
                    current_vertex = Some(p2);
                }
                Command::CubicTo(p1, p2, p3) => {
                    Cubic::new(current_vertex.unwrap(), p1, p2, p3).to_lines(1E-3, |edge| {
                        builder.vertex(edge.p0.into());
                        builder.vertex(edge.p1.into());
                        true
                    });
                    current_vertex = Some(p3);
                }
                Command::Close => {
                    let p0 = current_vertex.unwrap();
                    let p1 = initial_vertex.unwrap();
                    builder.vertex(p0.into());
                    builder.vertex(p1.into());
                    current_vertex = Some(p1);
                }
            }
            true
        });
        builder.end();
        builder.begin(gl::POINTS);
        builder.color(options.vertex_color);
        for &point in self.points() {
            builder.vertex(point.into());
        }
        builder.end();
    }
}

#[derive(Debug)]
pub struct App {
    event_loop: EventLoop<()>,
    windowed_context: ContextWrapper<PossiblyCurrent, Window>,
    builder_program: PrimitiveBuilderProgram,
}

impl App {
    pub fn new() -> App {
        let event_loop = EventLoop::new();
        let windowed_context = ContextBuilder::new()
            .build_windowed(
                WindowBuilder::new()
                    .with_title("viewer")
                    .with_inner_size(LogicalSize::new(512.0, 512.0)),
                &event_loop,
            )
            .unwrap();
        let windowed_context = unsafe { windowed_context.make_current().unwrap() };
        unsafe {
            gl::load_with(|symbol| windowed_context.get_proc_address(symbol) as *const _);
            gl::ClearColor(0.0, 0.0, 0.0, 1.0);
        }
        App {
            event_loop,
            windowed_context,
            builder_program: PrimitiveBuilderProgram::new(),
        }
    }

    pub fn run<F>(self, mut f: F)
    where
        F: 'static + FnMut(&mut PrimitiveBuilder),
    {
        self.event_loop.run({
            let windowed_context = self.windowed_context;
            let builder_program = self.builder_program;
            move |event, _, control_flow| match event {
                Event::LoopDestroyed => return,
                Event::WindowEvent { event, .. } => match event {
                    WindowEvent::Resized(physical_size) => windowed_context.resize(physical_size),
                    WindowEvent::CloseRequested => *control_flow = ControlFlow::Exit,
                    _ => (),
                },
                Event::RedrawRequested(_) => {
                    unsafe {
                        gl::Clear(gl::COLOR_BUFFER_BIT);
                        gl::PointSize(8.0);
                        gl::LineWidth(4.0);
                        gl::UseProgram(builder_program.program.0);
                    }
                    f(&mut PrimitiveBuilder {
                        attributes: builder_program.attributes,
                        mode: None,
                        color: [1.0, 1.0, 1.0],
                        vertices: Vec::new(),
                    });
                    unsafe {
                        gl::UseProgram(0);
                    }
                    windowed_context.swap_buffers().unwrap();
                    windowed_context.window().request_redraw();
                }
                _ => (),
            }
        });
    }
}

#[derive(Clone, Copy, Debug, Default)]
pub struct DrawOptions {
    pub vertex_color: [f32; 3],
    pub edge_color: [f32; 3],
    pub face_color: [f32; 3],
}

impl DrawOptions {
    pub fn new() -> DrawOptions {
        DrawOptions {
            vertex_color: [1.0, 0.5, 0.5],
            edge_color: [0.5, 1.0, 0.5],
            face_color: [0.5, 0.5, 1.0],
        }
    }
}

#[derive(Clone, Debug)]
pub struct PrimitiveBuilder {
    attributes: PrimitiveBuilderAttributes,
    mode: Option<GLenum>,
    color: [f32; 3],
    vertices: Vec<Vertex>,
}

impl PrimitiveBuilder {
    pub fn begin(&mut self, mode: GLenum) {
        assert!(self.mode.is_none());
        self.mode = Some(mode);
    }

    pub fn end(&mut self) {
        assert!(self.mode.is_some());
        unsafe {
            let mut vertex_array = 0;
            gl::GenVertexArrays(1, &mut vertex_array);
            gl::BindVertexArray(vertex_array);
            let mut vertex_buffer = 0;
            gl::GenBuffers(1, &mut vertex_buffer);
            gl::BindBuffer(gl::ARRAY_BUFFER, vertex_buffer);
            gl::BufferData(
                gl::ARRAY_BUFFER,
                (self.vertices.len() * mem::size_of::<Vertex>()) as GLsizeiptr,
                self.vertices.as_ptr() as *const c_void,
                gl::STATIC_DRAW,
            );
            gl::EnableVertexAttribArray(self.attributes.position);
            gl::VertexAttribPointer(
                self.attributes.position,
                2,
                gl::FLOAT,
                gl::FALSE,
                mem::size_of::<Vertex>() as GLsizei,
                &(*(0 as *const Vertex)).position as *const _ as *const c_void,
            );
            gl::EnableVertexAttribArray(self.attributes.color);
            gl::VertexAttribPointer(
                self.attributes.color,
                3,
                gl::FLOAT,
                gl::FALSE,
                mem::size_of::<Vertex>() as GLsizei,
                &(*(0 as *const Vertex)).color as *const _ as *const c_void,
            );
            gl::DrawArrays(self.mode.unwrap(), 0, self.vertices.len() as GLsizei);
            gl::BindVertexArray(0);
            gl::DeleteBuffers(1, &vertex_buffer);
            gl::DeleteVertexArrays(1, &vertex_array);
        }
        self.mode = None;
        self.vertices.clear();
    }

    pub fn color(&mut self, color: [f32; 3]) {
        self.color = color;
    }

    pub fn vertex(&mut self, position: [f32; 2]) {
        self.vertices.push(Vertex {
            position,
            color: self.color,
        })
    }
}

#[derive(Clone, Copy, Debug)]
#[repr(C)]
struct Vertex {
    position: [f32; 2],
    color: [f32; 3],
}

#[derive(Debug)]
struct Program(GLuint);

impl Program {
    fn new(vertex_shader: Shader, fragment_shader: Shader) -> Program {
        unsafe {
            let program = gl::CreateProgram();
            gl::AttachShader(program, vertex_shader.0);
            gl::AttachShader(program, fragment_shader.0);
            gl::LinkProgram(program);
            let mut status = 0;
            gl::GetProgramiv(program, gl::LINK_STATUS, &mut status);
            if status == 0 {
                let mut length = 0;
                gl::GetProgramiv(program, gl::INFO_LOG_LENGTH, &mut length);
                let mut log = Vec::with_capacity(length as usize);
                gl::GetProgramInfoLog(program, length, ptr::null_mut(), log.as_mut_ptr());
                log.set_len(length as usize);
                panic!(CStr::from_ptr(log.as_ptr()).to_str().unwrap());
            }
            Program(program)
        }
    }

    fn get_attribute(&self, name: &str) -> Option<GLuint> {
        let name = CString::new(name).unwrap();
        let attribute = unsafe { gl::GetAttribLocation(self.0, name.as_ptr()) };
        if attribute < 0 {
            return None;
        }
        Some(attribute as GLuint)
    }
}

impl Drop for Program {
    fn drop(&mut self) {
        unsafe {
            gl::DeleteProgram(self.0);
        }
    }
}

#[derive(Debug)]
struct PrimitiveBuilderProgram {
    program: Program,
    attributes: PrimitiveBuilderAttributes,
}

impl PrimitiveBuilderProgram {
    const VERTEX_SHADER_SOURCE: &'static str = r#"
        #version 100

        attribute vec2 aPosition;
        attribute vec3 aColor;

        varying vec3 vColor;

        void main() {
            gl_Position = vec4(aPosition, 0.0, 1.0);
            vColor = aColor;
        }
    "#;

    const FRAGMENT_SHADER_SOURCE: &'static str = r#"
        #version 100

        precision highp float;

        varying vec3 vColor;

        void main() {
            gl_FragColor = vec4(vColor, 1.0);
        }
    "#;

    fn new() -> PrimitiveBuilderProgram {
        let program = Program::new(
            Shader::new(
                gl::VERTEX_SHADER,
                PrimitiveBuilderProgram::VERTEX_SHADER_SOURCE,
            ),
            Shader::new(
                gl::FRAGMENT_SHADER,
                PrimitiveBuilderProgram::FRAGMENT_SHADER_SOURCE,
            ),
        );
        let attributes = PrimitiveBuilderAttributes {
            position: program.get_attribute("aPosition").unwrap(),
            color: program.get_attribute("aColor").unwrap(),
        };
        PrimitiveBuilderProgram {
            program,
            attributes,
        }
    }
}

#[derive(Clone, Copy, Debug)]
struct PrimitiveBuilderAttributes {
    position: GLuint,
    color: GLuint,
}

#[derive(Debug)]
struct Shader(GLuint);

impl Shader {
    fn new(r#type: GLenum, source: &str) -> Shader {
        unsafe {
            let shader = gl::CreateShader(r#type);
            gl::ShaderSource(
                shader,
                1,
                &CString::new(source).unwrap().as_ptr(),
                ptr::null(),
            );
            gl::CompileShader(shader);
            let mut status = 0;
            gl::GetShaderiv(shader, gl::COMPILE_STATUS, &mut status);
            if status == 0 {
                let mut length = 0;
                gl::GetShaderiv(shader, gl::INFO_LOG_LENGTH, &mut length);
                let mut log = Vec::with_capacity(length as usize);
                gl::GetShaderInfoLog(shader, length, ptr::null_mut(), log.as_mut_ptr());
                log.set_len(length as usize);
                panic!(CStr::from_ptr(log.as_ptr()).to_str().unwrap());
            }
            Shader(shader)
        }
    }
}

impl Drop for Shader {
    fn drop(&mut self) {
        unsafe {
            gl::DeleteShader(self.0);
        }
    }
}
