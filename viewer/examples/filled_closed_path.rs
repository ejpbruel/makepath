use makepath::geometry::Vector;
use makepath::{Mesh, Path, Tessellator};
use rand::Rng;
use viewer::{App, Draw, DrawOptions};

fn main() {
    let mut rng = rand::thread_rng();
    let mut path = Path::new();
    path.move_to(Vector::new(
        2.0 * rng.gen::<f32>() - 1.0,
        2.0 * rng.gen::<f32>() - 1.0,
    ));
    for _ in 1..10 {
        match rng.gen::<u32>() % 4 {
            0 => path.line_to(Vector::new(
                2.0 * rng.gen::<f32>() - 1.0,
                2.0 * rng.gen::<f32>() - 1.0,
            )),
            1 => path.quadratic_to(
                Vector::new(2.0 * rng.gen::<f32>() - 1.0, 2.0 * rng.gen::<f32>() - 1.0),
                Vector::new(2.0 * rng.gen::<f32>() - 1.0, 2.0 * rng.gen::<f32>() - 1.0),
            ),
            2 => path.conic_to(
                Vector::new(2.0 * rng.gen::<f32>() - 1.0, 2.0 * rng.gen::<f32>() - 1.0),
                Vector::new(2.0 * rng.gen::<f32>() - 1.0, 2.0 * rng.gen::<f32>() - 1.0),
                rng.gen::<f32>(),
            ),
            3 => path.cubic_to(
                Vector::new(2.0 * rng.gen::<f32>() - 1.0, 2.0 * rng.gen::<f32>() - 1.0),
                Vector::new(2.0 * rng.gen::<f32>() - 1.0, 2.0 * rng.gen::<f32>() - 1.0),
                Vector::new(2.0 * rng.gen::<f32>() - 1.0, 2.0 * rng.gen::<f32>() - 1.0),
            ),
            _ => unreachable!(),
        }
    }
    path.close();
    let mut vs = Vec::new();
    for _ in 0..path.points().len() {
        vs.push(Vector::new(
            0.001 * rng.gen::<f32>() - 0.0005,
            0.001 * rng.gen::<f32>() - 0.0005,
        ));
    }
    App::new().run(move |builder| {
        for (p, v) in path.points_mut().iter_mut().zip(vs.iter_mut()) {
            *p = Vector::new(p.x + v.x, p.y + v.y);
            if p.x < -1.0 {
                *p = Vector::new(-2.0 - p.x, p.y);
                *v = Vector::new(-v.x, v.y);
            }
            if 1.0 < p.x {
                *p = Vector::new(2.0 - p.x, p.y);
                *v = Vector::new(-v.x, v.y);
            }
            if p.y < -1.0 {
                *p = Vector::new(p.x, -2.0 - p.y);
                *v = Vector::new(v.x, -v.y);
            }
            if 1.0 < p.y {
                *p = Vector::new(p.x, 2.0 - p.y);
                *v = Vector::new(-v.x, -v.y);
            }
        }
        let mut mesh = Mesh::new();
        let mut tessellator = Tessellator::new();
        tessellator.tessellate(
            |f| {
                path.commands(f);
            },
            &mut mesh,
        );
        mesh.draw(builder, DrawOptions::new());
        path.draw(
            builder,
            DrawOptions {
                vertex_color: [0.5, 1.0, 1.0],
                edge_color: [1.0, 0.5, 1.0],
                ..DrawOptions::default()
            },
        );
    });
}
